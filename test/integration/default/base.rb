describe file('/etc/crontab') do
  its('type') { should cmp 'file' }
  it { should_not be_directory }
end

describe crontab('root') do
  its('commands') { should include 'helloworld  >> /var/log/helloworld.log 2>&1' }
end

if os.family == 'debian'
  describe service('cron') do
    it { should be_enabled }
    it { should be_running }
  end
elsif os.family == 'redhat'
  describe service('crond') do
    it { should be_enabled }
    it { should be_running }
  end
end
